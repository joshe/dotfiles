# Installing

.zshrc:

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

## Adding

```
cd $HOME
dotfiles add .tmux.conf
dotfiles commit -m "Add .tmux.conf"
dotfiles push
```

# Restoring

```bash
mkdir -p ~/dev/joshelson
cd ~/dev/joshelson
git clone --separate-git-dir=$HOME/.dotfiles git@gitlab.com:joshe/dotfiles.git  dotfiles
rsync --recursive --verbose --exclude '.git' dotfiles/ $HOME/
# Specify the preferences directory
defaults write com.googlecode.iterm2.plist PrefsCustomFolder -string "~/.iterm2_profile"
# Tell iTerm2 to use the custom preferences in the directory
defaults write com.googlecode.iterm2.plist LoadPrefsFromCustomFolder -bool true
```
